unity-IMGUI-Event

https://forum.unity.com/threads/hurr-eventtype-mouseup-not-working-on-left-clicks.99909/#post-1747810




EventType:


- MouseMove: 仅在没有鼠标按下时的鼠标移动才会被触发
- MouseDown: 鼠标按下(左中右键都会触发)
- MouseUp: 
- MouseDrag: 拖拽时会触发,不论MouseDown时是否 吃掉事件





- 可以吃掉的事件

MouseDown, MouseDrag

MouseUp 千万不要使用 event.Use(), 会影响后续的逻辑






#### 不能触发MouseUp事件

鼠标的MouseUp事件跟 GUIUtility.hotControl 相关,

使用方式:

~~~
// GUIUtility.GetControlID性能较低,所以不要频繁调用,要缓存起来
var controlID = GUIUtility.GetControlID("my custom string".GetHashCode(), FocusType.Passive);


var evt = Event.current;
switch(evt.type)
{
    case EventType.MouseDown:
        if (CanDrag())
        {
            evt.Use(); // 防止Unity内置工具使用
            // 必须要重新设置自己的hotControl,否则不能触发MouseUp
            GUIUtility.hotControl = controlID;
            // 做自己的其他事情
        }
        break;
    case EventType.MouseUp:
        // MouseUp里不需要重新设置GUIUtility.hotControl,系统会自动重置
        // MouseUp里也不要使用 evt.Use(); 会影响Unity后续的流程
        // 做自己的事情
        break;
    case EventType.MouseDrag:
        // 处理鼠标左键,并且判断是否是自己关心的hotControl
        if(evt.button == 0 && GUIUtility.hotControl == controlID)
        {
            evt.Use();
            // 做自己的事情
        }
}
~~~



IMGUI 坐标系

左上角为(0,0), 右下角为(n,n)



~~~
// IMGUI 坐标转为World坐标
private Vector3 Gui2World(Vector2 uiPos, Camera camera)
{
    uiPos.y = size.y-uiPos.y - 20;
    uiPos.x = Mathf.Clamp(uiPos.x, 0, size.x);
    uiPos.y = Mathf.Clamp(uiPos.y, 0, size.y);
    return camera.ScreenToWorldPoint(new Vector3(uiPos.x, uiPos.y, 500));
}
~~~