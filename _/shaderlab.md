
https://docs.microsoft.com/en-us/windows/desktop/direct3dhlsl/dx-graphics-hlsl-function-parameters

#### function params InputModifier

参数的修饰符

| Flag    | Description             |
| in      | Input only (default)    |
| out     | Output only             |
| inout   | Input and an output     |
| uniform | Input only constant data|


~~~
void func(inout fixed4 col) {
    col = fixed(1,1,1,1);
}
~~~


#### 常用函数


tex2D(sampler2D tex, float2 uv)
对纹理采样,获取uv坐标下的颜色
仅用于 frag



tex2DLod(sampler2D tex, float4(uv, lodx lody))
对纹理采样,获取uv坐标下的颜色
可用于 vert



~~~
struct v2f
{
    // SV_POSITION 必须在第一个，否则android上不能正常编译，
    half4 vertex : SV_POSITION;
    half2 uv : TEXCOORD0;
    half4 bumpCoords:TEXCOORD1;
    half3 viewVector:TEXCOORD2;

};
~~~



#### 技巧

1. 避免使用 if/for 等分支语句  	(分支会破坏GPU的并行性)
2. for循环常量次数可以使用 		(Unity会优化)
