
Assert

https://docs.unity3d.com/ScriptReference/Assertions.Assert.html


unity工程:
debug

非unity工程:

需要设置条件选项: `UNITY_ASSERTIONS`



~~~
Assert.raiseExceptions = true;
~~~

raiseExceptions 默认为false,
如果设置了则会抛出异常,否则只是打印日志








对于错误处理我觉得有2中情况，
1. 逻辑上做容错， 比如判空等，防止报错
2. 使用断言来主动报错

当逻辑本身没有错误，而是输入有错（比如配置表错误），需要修改， 这时适用于第2中情况，尽早出错，提醒这里有问题，需要修改。
当然不论哪种情况都不允许界面卡死

这是我的理解。


这是从Const表里配出来的，逻辑已经确定了，如果报错表示策划表配置错了，需要修改配置表，
Assert.IsNotNull(tdItem); 我在前面做了断言。 这里需要再把断言加强，明确具体的错误信息。
