lightmap

https://docs.unity3d.com/Manual/Lightmaps-TechnicalInformation.html

lightmap使用uv1来存放lightmap的纹理坐标

unity_LightmapST: lightmap纹理的缩放和偏移

fixed3 DecodeLightmap(fixed3) 解码lightmap颜色


~~~
// vertex shader input data
struct appdata
{
    float3 pos : POSITION;
    float3 uv0 : TEXCOORD0;
    float3 uv1 : TEXCOORD1;  // lightmap
};

// vertex-to-fragment interpolators
struct v2f
{
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;  // lightmap
    float4 pos : SV_POSITION;
};


// vertex shader
v2f vert(appdata IN)
{
    v2f o;
    // compute texture coordinates
    o.uv0 = IN.uv0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    o.uv1 = IN.uv1.xy * unity_LightmapST.xy + unity_LightmapST.zw;

    // transform position
    o.pos = UnityObjectToClipPos(IN.pos);
    return o;
}

// textures
sampler2D _MainTex;

// fragment shader
fixed4 frag(v2f IN) : SV_Target
{
    // Fetch color texture
    fixed4 col = tex2D(_MainTex, IN.uv0.xy);

    // Fetch lightmap
    half4 bakedColorTex = UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.uv1.xy);
    col.rgb *= DecodeLightmap(bakedColorTex);

    col.a = 1;
    return col;
}
~~~


https://forum.unity.com/threads/important-information-regarding-mesh-uv-channels.370746/#post-2402221
https://docs.google.com/document/d/1gUU02-_R7euDN7X12LMnnXLvmt1ELFUepkXsac2T5Mc/edit#
unity 不同uv的使用

| Ordinal | Mesh class property | Shader Code | Used for                 |
|---------|---------------------|-------------|--------------------------|
| First   | mesh.uv             | TEXCOORD0   | Diffuse, Metal/Spec etc. |
| Second  | mesh.uv2            | TEXCOORD1   | Baked lightmap           |
| Third   | mesh.uv3            | TEXCOORD2   | Realtime GI data         |
| Fourth  | mesh.uv4            | TEXCOORD3   | Whatever you like        |
