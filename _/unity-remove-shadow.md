移除阴影


### 不产生阴影

1. MeshRenderer 中 设置 CastShadows 为 Off (单个对象)
2. 自定义shadow禁用 `FallBack "Diffuse"`   (所有应用该shader的对象)
3. QualitySettings中设置Shadows为 Disable Shadows (所有对象)

### 不接受阴影

1. MeshRenderer 中 取消勾选 Receive Shadows (单个对象)
2. 
3. QualitySettings中设置Shadows为 Disable Shadows (所有对象)









https://forum.unity.com/threads/fallback-off-turns-off-shadows-in-surface-shader.257430/#post-1703672