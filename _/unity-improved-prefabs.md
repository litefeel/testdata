

#### Prefab Mode change prefab without SerializedObject

修改之前先调用`UnityEditor.Undo.RecordObject(Object objectToUndo, string name)`，那么这次修改才能被Unity检测到修改，否则不会自动保存，也不能通过手动保存来保存更改。


#### 当前是否在Prefab模式下

~~~
// 当前是否处于Prefab模式
var isPrefabMode = PrefabStageUtility.GetCurrentPrefabStage() != null;
// 当前是否处于某个Prefab的Prefab模式
var isSpecificPrefabMode = PrefabStageUtility.GetPrefabStage(gameObject) != null;
~~~
