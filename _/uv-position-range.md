UV坐标的取值范围


UV坐标的取值范围为0-1,其实应该为 [0,1) 包含0,不包含1,

通常用clamp(uv),只适用于 wrap 模式为 Clamp的纹理,对于 Repeat模式的纹理不可以将uv坐标 clamp到[0,1]


对于Clamp模式的纹理, UV大于等于1时,将适用最右/最上的像素
对于Repeat模式的纹理, UV大于1时 使用 %1 来计算真实的uv坐标


Clamp(uv) 其实没有必要



#### 简单验证UV坐标
1. 将一张贴图分为4块, 4个角分别涂上不同的颜色
2. 导入Unity后设置 WrapMode为Repeat, FilterMode为Point
3. 修改shader,强制纹理采样的坐标, 尝试将uv坐标设置为0 1 0.9的不同组合形式
4. 发现 uv坐标的1.0 在 Repeat模式下为0的纹理

