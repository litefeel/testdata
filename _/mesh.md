mesh

https://answers.unity.com/questions/423569/meshvertices-is-too-small.html
https://docs.unity3d.com/ScriptReference/Mesh.html



#### 更新mesh流程

1. Mesh.Clear()   如果三角形数量改变了必须调用
2. Mesh.SetVertices
3. Mesh.SetUVs
4. Mesh.SetColors  可选
5. Mesh.SetTriangles
6. Mesh.RecalculateBounds()   改变了位置则需要重新计算
