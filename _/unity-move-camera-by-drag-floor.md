---
post_title: Unity中通过拖拽地板移动摄像机
post_name: unity-move-camera-by-drag-floor
layout: post
published: false
tags:
  - Unity
  - Camera
  - Move
categories:
  - Unity
---



#### 拖拽地板移动摄像机





#### 松开后地板惯性移动


原理: 通过计算鼠标最后一次移动的速度计算地板应移动的距离.

鼠标最后一次移动的速度向量 * 惯性倍数, 计算地板目标点.
注意摄像机不应该看向最后的鼠标所在点的屏幕坐标计算世界坐标,因为是移动向量,所以只需要计算最后一次的鼠标移动向量