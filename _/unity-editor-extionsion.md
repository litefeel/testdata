

在场景中画东西



#### Gizmos

UnityEngine.Gizmos类的API

使用位置, MonoBehaviour
OnDrawGizmos/OnDrawGizmosSelected


> Gizmos.DrawMesh requires a mesh with positions and normals

Gizmos.DrawMesh的Mesh必须有normals和position


Scene视图中画大量方块， Gizmos.DrawMesh效率最高
Mesh注意三角形顺序为 顺时针方向才能显示，否则不能显示


#### Debug.DrawXXX

任何位置



#### GUI

OnGUI/
OnInspectorGUI

如果需要在SceneView中显示,那么必须用 Handles.BeginGUI()/EndGUI()包起来,否则不显示



#### Handles

SceneView中

画GUI相关的东西需要
OnSceneView

OnSceneView