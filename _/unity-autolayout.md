unity 自动布局

1. 布局的限制


1. 不能根据父对象的尺寸来设置该节点的大小,除非父节点也是布局的一部分.



### 布局流程 Rebuild 

1. 自底向上 度量ILayoutElement的宽度
2. 自顶向下 设置ILayoutController的宽度(相同节点优先执行ILayoutSelfController)
3. 自底向上 度量ILayoutElement的高度
4. 自顶向下 设置ILayoutController的高度(相同节点优先执行ILayoutSelfController)




1. Rebuild是一次性的,用过即失效,

// 标记要应用自动布局,只会处理 ILayoutGroup
LayoutRebuilder.MarkLayoutForRebuild(rectTransform);


### 触发 RebuildLayout

调用 `LayoutRebuilder.MarkLayoutForRebuild(rectTransform)` 来注册Rebuild,

而该方法在UGUI中被普遍调用.

MarkLayoutForRebuild 被用在UGUI的以下情景

1. OnEnable/OnDisable (激活/禁用)
2. OnBeforeTransformParentChanged (改变父节点之前,保证之前的父节点正常)
3. OnTransformParentChanged (改变父节点之后,保证之后的父节点正常)
4. OnRectTransformDimensionsChange 尺寸改变
5. OnDidApplyAnimationProperties UGUI的动画属性变化(比如按钮状态转变的动画)
6. 其他一些Layout组件的属性设置时会触发

尽管有很多的地方会触发`MarkLayoutForRebuild`,但只有以下3个条件同时满足时才能**真正触发**LayoutRebuild

1. rectTransform 是 ILayoutGroup
2. rectTransform 的直接父节点是 ILayoutGroup
3. ILayoutGroup 激活并启用 (isActiveAndEnabled)



https://docs.unity3d.com/Manual/UIAutoLayout.html





#### SetLayoutDirty

触发布局重建， 只有父节点有 ILayoutGroup 或当前节点有ILayoutController才会标记为布局重建


#### SetVerticesDirty/SetMaterialDirty

网格重建、







#### RectMask2D
每帧都会执行，不论有没有变化（有变化执行的多，没变化执行的少）

优化点：
RectMask2D尽量不要嵌套
RectMask2D尽量不要太深（尤其不要嵌套太多Canvas下）
