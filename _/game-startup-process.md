game-startup-process.md


1. 资源检查
2. 


资源检查

1. 检查是否需要解压缩资源到外部磁盘()
2. 解压缩资源到外部磁盘


实现：
app中存有appResVer（app打包完成将不能被修改）
外部存储中有当前的sdResVer(不存在则为0)
如果appResVer>sdResVer，则删除外部存储中的资源，并把当前app中的资源解压出来


app中记录 appResVer/appClientVer，这两个都是只读，打包时固化。
外部存储中有 sdResVer/sdClientVer, 会跟随client和资源的版本增长


1. 从服务器获取最新版本号（clientVer+resVer)
2. 如果clientVer > appClientVer，则提示强更（可以兼容更新）








**兼容版本更新**

一个热更版本想要兼容多个客户端版本的做法：


version: major.minor.patch （主版本.次版本.修订版本）


只有当clientVer主版本号更新的时候，才必须强更，其他版本号修改时可更可不更

1. 当资源更新的时候资源版本号总是会提升
2. 当资源必须依赖新版客户端时，则提升clientVer的major
3. 资源更新后也可能需要打新的客户端，但不提升major，只提升minor/patch



#### 术语

clientVer： 客户端版本号，通常是强更版本号（apk/ipa）
resVer: 资源版本号