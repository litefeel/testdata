unity-baseLayout



RectTransform




anchor: 锚点
provit: 轴点

achoterPosition


锚点的作用：
当父节点尺寸变化的时候，当前节点以何种方式来跟随，
例如，锚点为左上角，那么不论父节点尺寸如何变化，该节点总是在父节点的左上角。


锚点分两种情况：
1. 单一轴上min=max， 当前节点的位置会跟随父节点，当前节点尺寸不变
2. 单一轴上min!=max，当前节点的位置不变，尺寸会跟随父节点的尺寸变化而变化


轴点的作用：
当前节点绕轴点旋转，以轴点为中心缩放。



achoterPostion