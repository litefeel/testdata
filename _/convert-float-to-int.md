convert-float-to-int.md


(int)f Math.Floor 仅在 非负数的情况下等同


当负数时， (int)f == Math.Ceil

f = 0.1f
(int)f;  // 0
Math.Floor(f); // 0


f = -0.1f
(int)f; // 0
Math.Floor(f); // -1
