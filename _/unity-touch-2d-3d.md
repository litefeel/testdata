---
post_title: Unity中鼠标处理
post_name: unity-touch-2d-3d
layout: post
published: false
tags:
  - Unity
  - Touch
categories:
  - Unity
---

UGUI系统的EventSystem

#### UGUI中的鼠标处理

UnityEngine.UI中已有的可交互组件
~~~
Button.onClick.AddListener()
Toggle.onValueChanged.AddListener()
...
~~~


UnityEngine.EventSystem.IEventSystemHandler的子接口

~~~
IPointerUpHandler
IPointerDownHandler
IPointerClickHandler
IPointerEnterHandler
IPointerExitHandler
IInitializePotentialDragHandler
IBeginDragHandler
IDragHandler
IDropHandler
IEndDragHandler
...
~~~


#### 3D对象的鼠标处理

~~~
OnMouseDown
OnMouseDrag
OnMouseEnter
....
~~~


#### 检测鼠标是否在UGUI上

~~~
private static List<RaycastResult> raycastResults = new List<RaycastResult>(); 

public static bool IsOnGui()
{
    var eventSystem = EventSystem.current;
    if (Input.touchCount > 0)
    {
        for (var i = 0; i < Input.touchCount; i++)
        {
            var touch = Input.GetTouch(i);
            if (eventSystem.IsPointerOverGameObject(touch.fingerId))
                return true;
        }

        PointerEventData eventData = new PointerEventData(EventSystem.current);
        for (var i = 0; i < Input.touchCount; i++)
        {
            var touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began || 
                touch.phase == TouchPhase.Ended || 
                touch.phase == TouchPhase.Canceled)
            {
                eventData.position = Input.mousePosition;
                EventSystem.current.RaycastAll(eventData, raycastResults);
                bool found = false;
                foreach (var result in raycastResults)
                {
                    if (result.gameObject)
                    {
                        found = true;
                        break;
                    }
                }
                raycastResults.Clear();
                if (found) return true;
            }
        }
    }
    else if(Input.mousePresent)
    {
    	// 鼠标左键
        return EventSystem.current.IsPointerOverGameObject();
    }
    return false;
}
~~~