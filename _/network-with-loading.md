网络层上层设计



push: 后端消息推送到前端

notify: 对后端的通知,不需要后端返回,也不关心后端是否有收到(通常不用)

RPC: request/response, 请求后端,并且需要后端返回


push 和 notify 容易,不需要loading,纯逻辑

RPC设计

1. request/response 协议ID必须是一一对应,也只能一对一,否则协议使用的地方多了就会很难处理
2. request/


客户端:

1. 构造一个request/response队列


把消息封装成RPC,这样request/response就变成成对出现,再处理下超时情况,
对于菊花loading
记录request队列,当response或者超时时才把request队列里相应的message id给去除掉,当request队列里有需要显示菊花时显示菊花,否则取消菊花



// 网络loading的处理逻辑

~~~ csharp
struct RPPair { int request, int response, bool isLoading, }
List<RPPair> list = new List<RPPair>();
int loadingCount = 0;


// request
void request(int requestId, bool isLoading, ...) {
    int responseId = getResponseId(requestId);
    list.Add(new PRPair(requestId, responseId, isLoading))
    if (isLoading) {
        increaseLoading();
    }
    // 后续的逻辑
}

// 网络正常返回
void onResponse(int responseId, ...) {
    RPPair pair = getAndPopResponse(responseId);
    if(pair != null && pair.isLoading) {
        decreaseLoading();
        
    }
    // 后续的逻辑
}
// 网络超时
void onRpcTimeout(int requestId) {
    RPPair pair = getAndPopRequest(responseId);
    if(pair != null && pair.isLoading) {
        decreaseLoading();
    }
    // 后续逻辑
}

// 增加loading
void increaseLoading() {
    loadingCount++;
    // 显示网络loading
}
// 减少loading
void decreaseLoading() {
    loadingCount--;
    assert(loadingCount >= 0);
    if (loadingCount == 0) {
        // 隐藏网络loading
    }
}
~~~


服务器处理:

1. request/response必须一一对应
2. 仅在收到request里函数里发送response,其他逻辑处理可以分到不同的函数中
3. A模块收到的request,处理后调用B模块,禁止B模块发送response
4. 



