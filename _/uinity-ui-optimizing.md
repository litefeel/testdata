uinity-ui-optimizing



1. 尽量使用全屏界面（全屏界面时可以把3D渲染停掉）
1. 界面中显示静止3D模型时，渲染到RenderTexture，并只渲染一次后把摄像机停掉，防止每帧都去渲染
1. 动静分离，总是更新的元素（血条等）与普通的界面分拆到不同的Canvas中
1. 合并图集，常用的UI元素合并到一张纹理集中
1. 禁用不需要被点击的Image/RawImage的RaycastTarget




#### 降低填充率

1. 尽量隐藏不可见的元素
1. 简化UI Shader
1. 背景有花边时，将花边和背景合并到一个Image中，而非组合的方式显示背景和花边
1. 九宫格Image时，如果内容被遮挡，则取消勾选 FillCenter
1. 九宫格Image时，ImageType禁止使用Tiled，应使用Sliced
1. 不要因为需要较大的点击区域而让把UI周围填充透明像素，而是新增无渲染的触摸区域组件



#### Text

1. 不同字形/类型/Size 都将生成不同的纹理（32号24号字符会生成不同的字符纹理，虽然在同一张纹理集中）
1. 尽量使用统一的字符Size，而不是随便什么Size（减少动态生成的字符纹理个数）
1. 预先分别字体纹理的内容，防止运行时不停的重建字体纹理集，使用`Font.RequestCharactersInTexture`，动态字体必须在运行时申请










#### RectMask2D vs Mask

Mask 本身会增加一个drawcall，

RectMask2D 本身不会增加DC，但是会打破batch

|      /     | RectMask2D | Mask |
|:----------:|:----------:|:----:|
| 本身产生DC |     否     |  是  |
| 打破batch  |     是     |  是  |
| 消耗CPU    |     是     |  否  |
| 消耗GPU    |     否     |  否  |